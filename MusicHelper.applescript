//#!/usr/bin/env osascript -l JavaScript

/**
    Discord-iTunes Helper
    Copyright (C) 2019  Felix O.
	felix@von-oertzen-berlin.de
	
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
	
iTunes is a trademark of Apple Inc., registered in the U.S. and other countries.
Discord is a trademark, which may is registered in the U.S. and other countries.

	*/

function musicActive() {
	var app = new Application('System Events')
	var list = app.applicationProcesses()
	for (var x = 0; x < list.length; x++) {
		if (list[x].name() == "Music") {
			return true;
		}
	}
	return false;
}

function main () {
	if (musicActive()) {
		try {
  			var music = new Application('Music')
  			var currentTrack = music.currentTrack
  			var output = {
    			name: currentTrack.name(),
    			artist: currentTrack.artist() == '' ? "Radio" : currentTrack.artist(),
    			position: Math.round(music.playerPosition() * 100) / 100,
    			duration:  Math.round(currentTrack.duration() * 100) / 100,
				playbackstate: music.playerState()
  			}
  			if (currentTrack.name() == currentTrack.artist() === '') {
    			return "-1";
  			}
  			return JSON.stringify(output, null, 2)
		} catch (err) {
		return "-1";
		}
	} else {
		return "-2";
	}
}

main()