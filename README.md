![AppleScript](https://img.shields.io/badge/using-AppleScript-green.svg?logo=apple)
![Python](https://img.shields.io/badge/using-Python%203-blue.svg?logo=python)
![License](https://img.shields.io/badge/License-GNU%20General%20Public%20License%20v3.0-green.svg?logo=gnu)

[![pypresence](https://img.shields.io/badge/using-pypresence-00bb88.svg?logo=discord&logoWidth=20)](https://github.com/qwertyquerty/pypresence)

# Discord-iTunes Helper
## General
This scripts let you show your playback state in Discord, like Spotify does.

You are able to show the artist, the title and the time until the actual song ends.

### Attention: 
This script only works for macOS Catalina and above, because iTunes was
renamed in Catalina, so therefore the api name changed.
If you need assistance for macOS Mojave and below, please contact me. 

## Requirements
Primary you will need any Apple-Computer.

Futhermore you will need the python interpreter which you will get at the
[Python Homepage](https://www.python.org)

and the following python module(s) which you don't need to install by hand:
*  pypresence

## How can I use it?
You're able to install this script with a simple script: setup.py

You need to
*  clone the project
*  run setup.py
*  run it.

For a TKinter Window (a real window) please start main.py. If you
prefer a command line tool, use console.py.
### What is the client id?
Every Application for Discord has an id for accessing the api.
In order to get an own id, you need to create your own application at [https://discordapp.com/developers/applications/](https://discordapp.com/developers/applications/).

There you will get your Client ID for accessing the "rich presence api".

## What's on the roadmap?
*  simplify usage (maybe a simple UI)
*  (maybe) pack the scripts as *.app file


iTunes is a trademark of Apple Inc., registered in the U.S. and other countries.

Discord is a trademark, which may is registered in the U.S. and other countries.
